<script>
$(function () {

    var content = {
        'title': '<div > <h3> Alert</h3> </div>',
        'alert': '<div>' +
        '<div> This is a simple alert.</div>' +
        '<div> with some <strong>HTML </strong> contents</div>' +
        '<button id="ok">OKAY</button>' +
        '</div>'
    };
    var small_div = $('<div>' + '</div>');
    small_div.css({
        margin: '50px auto',
        width: '400px',
        height: '150px',
        background: '#ddd',
        color: '#000',
        border: '10px',
        padding: '15px'
    });

    function getAlert(content) {
        var big_div = $('.big_div');
        small_div.html(content.title);
        small_div.html(content.alert);
        big_div.html(small_div);
        $('#ok').on('click', function () {
            big_div.animate({opacity: 0, top: '45%'}, 200);
        });
    }
    $('.action').on('click', function () {
        $('.big_div').css({
            width: '100%',
            height: '100%',
            background:'gray',
            left: '0',
            top: '0',
            position: 'fixed',
            opacity: '100'
        });
        getAlert(content);
    });
});
</script>
