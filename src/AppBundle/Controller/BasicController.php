<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Institution;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class BasicController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction(Request $request)
    {
        $session= $this->get('session');
        $session->set('bucket', [0]);

        $institution = $this->getDoctrine()
            ->getRepository('AppBundle:Institution')
            ->findAll();

        return $this->render('Main/main_page.html.twig', [
            'institutions' => $institution
        ]);
    }

    /**
     * @Route("/details/{institution_id}", requirements={"institution_id": "\d+"})
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param $institution_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsAction(Request $request, $institution_id)
    {
        $session= $this->get('session');
        $lists= $session->get('bucket');

        $form_bucket = $this->createForm('AppBundle\Form\BucketType');
        $institution = $this->getDoctrine()
            ->getRepository('AppBundle:Institution')
            ->find($institution_id);

        $total_price = 0;
        $dishes = $institution->getDishes();

        if ($session->has('bucket')){
            $lists = $session->get('bucket');
            if (isset($lists[$institution_id])){
                $bucket_for_institution = $lists[$institution_id];
                foreach ($bucket_for_institution as $item){
                       $total_price += $item['sum_price'];
                }
            }
        }

        return $this->render('Main/detail.html.twig', [
            'dishes' => $dishes,
            'institution_name' => $institution->getName(),
            'description' => $institution->getDescription(),
            'form_bucket'=> $form_bucket,
            'lists' => $lists,
            'total_price' => $total_price
         ]);
    }

    /**
     * @Route("/add_to_bucket/{dish_id}", requirements={"dish_id": "\d+"})
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param $dish_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addBucketAction(Request $request, $dish_id)
    {
        $dish = $this->getDoctrine()
            ->getRepository('AppBundle:Dish')
            ->find($dish_id);
        /** @var Institution $institution */
        $institution = $dish->getInstitution();

        $form  = $this->createForm('AppBundle\Form\BucketType');
        $form->handleRequest($request);

        $session= $this->get('session');

        $count = 0;
        if($form->isSubmitted()){
            $count = $form->getData()['count'];
        }
        $session->set('count',$count );

        $sum_price = $dish->getPrice() * $session->get('count');
        $session->set('dish_name', $dish->getName());

            $bucket  = [
                'dish'=> $session->get('dish_name'),
                'price' => $dish->getPrice(),
                'sum_price' => $sum_price,
                'count'=>$session->get('count'),
            ];

        $bucket_session = $session->get('bucket');
        $bucket_session[$institution->getId()] [] = $bucket;
        $session->set('bucket', $bucket_session);

        return $this->redirecttoRoute('app_basic_details', array(
            'institution_id' => $institution->getId(),
        ));
    }

}
