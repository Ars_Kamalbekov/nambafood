<?php
/**
 * Created by PhpStorm.
 * User: arsen
 * Date: 22.07.17
 * Time: 16:07
 */
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class LoadArticleData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user
            ->setEmail('original@some.com')
            ->setEnabled(true)
            ->setRoles(['ROLE_ADMIN'])
            ->setUsername('Admin_01')
            ->setName('Admin');
        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($user, '123321');
        $user->setPassword($password);

        $manager->persist($user);
        $manager->flush();
    }
}
